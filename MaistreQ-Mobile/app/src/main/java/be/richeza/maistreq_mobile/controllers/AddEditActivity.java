package be.richeza.maistreq_mobile.controllers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.models.Recipe;
import be.richeza.maistreq_mobile.models.RecipeBook;
import be.richeza.maistreq_mobile.models.helpers.ApiClientHandler;
import be.richeza.maistreq_mobile.models.helpers.PictureFormatter;
import be.richeza.maistreq_mobile.models.helpers.interfaces.Callback;
import be.richeza.maistreq_mobile.models.helpers.ApiMiddleware;
import be.richeza.maistreq_mobile.models.helpers.JsonHelper;
import be.richeza.maistreq_mobile.models.helpers.StringFormatter;

/**
 * This is the controller that will handle the adding or the editing of a recipe
 */

public class AddEditActivity extends BaseActivity {

    public static final String TAG = "AddEditActivity";
    public static int PHOTO_CODE = 555;
    private static final int CAMERA_CODE = 333;

    // Members
    private String mRecipeID;
    private Recipe mEditingRecipe;
    private File tempPic;

    // Api handler
    private ApiMiddleware mApiMiddleware;

    // Widgets
    private EditText mRecipeTitle;
    private EditText mRecipeIngredients;
    private EditText mRecipeMethod;
    private Button mSaveRecipeButton;
    private Button mCancelRecipeButton;
    private Button mStartRecordButton;
    private Button mStopRecordButton;
    private TextView mRecordLabel;
    private Button mTakePictureButton;

    /**
     * Defining this callback as the default one used by the save button
     */
    private Callback callback = new Callback() {
        @Override
        public void onDataReceived(JSONObject object) {
            Log.d(TAG, object.toString());
            try{
                if(object.getInt("code") == 200){
                    Toast.makeText(AddEditActivity.this, getString(R.string.item_saved_rsc), Toast.LENGTH_LONG).show();


                    if(tempPic != null){
                        // Convert pic to base 64 string
                        String imgStr = PictureFormatter.ConvertToBase64(tempPic);

                        RequestParams params = new RequestParams();
                        params.put("recipeImg", imgStr);

                        mApiMiddleware.addImage(mRecipeID, params, new Callback() {
                            @Override
                            public void onDataReceived(List<Recipe> recipes) {
                                return;
                            }

                            @Override
                            public void onDataReceived(JSONObject object) {
                                tempPic.delete();
                                tempPic = null;
                                Log.d(TAG, "Picture sent");
                            }

                            @Override
                            public void onDataReceived(byte[] bytes) {
                                return;
                            }

                            @Override
                            public void onDataReceived(Bitmap bitmap) {
                                return;
                            }

                            @Override
                            public void onFailure(JSONObject object) {
                                Log.d(TAG, "FAIL\n" + object.toString());
                            }

                            @Override
                            public void onFailure(byte[] bytes) {
                                return;
                            }
                        });
                    }
                    onBackPressed();
                }
            }catch(JSONException e){
                Log.e(TAG, "Cannot save to API");
                e.printStackTrace();
            }
        }

        @Override
        public void onDataReceived(byte[] bytes) {
            return;
        }

        @Override
        public void onDataReceived(Bitmap bitmap) {
            return;
        }

        @Override
        public void onFailure(JSONObject object) {
            // throwed
            return;
        }

        @Override
        public void onFailure(byte[] bytes) {
            return;
        }

        @Override
        public void onDataReceived(List<Recipe> recipes) {
            return;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_view);

        // Setup the widgets that we will manipulate
        this.setUpWidgets();

        if(savedInstanceState != null){
            mRecipeID = savedInstanceState.getString("recipe_id");
        } else {
            // get Intent info
            mRecipeID = getIntent().getStringExtra("recipe_id");
        }

        // Initialize the Api handler and setup my JSON response handler
        this.mApiMiddleware = new ApiMiddleware(this);

        // If the ID is null we're adding a new recipe, if not, we're modifying an already existing one
        if(mRecipeID != null){
            mEditingRecipe = RecipeBook.get(this).getRecipe(mRecipeID);
            setTitle("Editing " + mEditingRecipe.getRecipeTitle());
            setUpView(true);
        } else{
            setTitle("Adding a new recipe");
            setUpView(false);
        }
    }

    /**
     * Saves the ID of the recipe we're modifying
     * @param outState : Bundle -> The bundle that will be recovered at the recreation of the activity
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("recipe_id", mRecipeID);

    }

    /**
     * Initialize widgets
     */
    private void setUpWidgets() {
        this.mRecipeTitle = findViewById(R.id.recipe_title_TV);
        this.mRecipeIngredients = findViewById(R.id.recipe_ingredients_TV);
        this.mRecipeMethod = findViewById(R.id.recipe_content_TV);
        this.mSaveRecipeButton = findViewById(R.id.save_recipe_button);
        this.mCancelRecipeButton = findViewById(R.id.cancel_recipe_button);
        this.mStartRecordButton = findViewById(R.id.start_record_button);
        this.mStopRecordButton = findViewById(R.id.stop_record_button);
        this.mRecordLabel = findViewById(R.id.record_label);
        this.mTakePictureButton = findViewById(R.id.take_a_pic_btn);
    }

    /**
     * Set up some widgets' attribute(s) based on the fact that we are editing an existing recipe or adding a new one.
     * @param isEditing : Boolean -> Determined by the fact that we are editing a recipe or not.
     */
    private void setUpView(boolean isEditing){
        if(isEditing){
            this.mRecipeTitle.setHint(mEditingRecipe.getRecipeTitle());
            this.mRecipeIngredients.setHint(StringFormatter.formatListToString(mEditingRecipe.getRecipeIngredients()));
            this.mRecipeMethod.setHint(StringFormatter.formatListToString(mEditingRecipe.getRecipeMethod()));

            /**
             * Saves the recipe online
             */
            this.mSaveRecipeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mApiMiddleware.updateRecipe(getValues(), mEditingRecipe.getObjectID(), callback);
                }
            });

            /**
             * Goes back to the detailed view
             */
            this.mCancelRecipeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AddEditActivity.this, DetailedViewActivity.class);
                    intent.putExtra("recipe_id", mEditingRecipe.getObjectID());
                    startActivity(intent);
                    AddEditActivity.this.finish();
                }
            });

            /**
             * Toast that is showed in case of failures
             */
            final Toast failureToast = Toast.makeText(AddEditActivity.this, "Cannot start the record", Toast.LENGTH_LONG);

            /**
             * Starts the recording of the video
             */
            this.setUpStartRecordBtn(failureToast);

            /**
             * Stops the recording of the video
             */
            this.setUpStopRecordBtn(failureToast);

        }
        else {
            this.mStartRecordButton.setVisibility(View.GONE);
            this.mStopRecordButton.setVisibility(View.GONE);
            this.mRecordLabel.setVisibility(View.GONE);

            /**
             * Adds the recipe online
             */
            this.mSaveRecipeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mApiMiddleware.addRecipe(getValues(), callback);
                }
            });

            /**
             * Cancels the recipe
             */
            this.mCancelRecipeButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mTakePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        requestPermissions(new String[] { Manifest.permission.CAMERA }, CAMERA_CODE);
                    } else {
                        takePicture();
                    }
                }
            }
        });
    }

    /**
     * Sets up the button that will ask for starting the record
     * @param failureToast : Toast -> toast that is shown when the request fails
     */
    private void setUpStartRecordBtn(final Toast failureToast){
        this.mStartRecordButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mApiMiddleware.startRecord(mEditingRecipe.getObjectID(), new Callback() {
                    @Override
                    public void onDataReceived(List<Recipe> recipes) {
                        return;
                    }

                    @Override
                    public void onDataReceived(JSONObject object) {
                        Toast.makeText(AddEditActivity.this, "Recording started", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onDataReceived(byte[] bytes) {
                        return;
                    }

                    @Override
                    public void onDataReceived(Bitmap bitmap) {
                        return;
                    }

                    @Override
                    public void onFailure(JSONObject object) {
                        failureToast.show();
                    }

                    @Override
                    public void onFailure(byte[] bytes) {
                        return;
                    }
                });
            }
        });
    }

    /**
     * Sets up the button that will ask for stopping the record
     * @param failureToast : Toast -> toast that is shown when the request fails
     */
    private void setUpStopRecordBtn(final Toast failureToast){
        this.mStopRecordButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mApiMiddleware.stopRecord(mEditingRecipe.getObjectID(), new Callback() {
                    @Override
                    public void onDataReceived(List<Recipe> recipes) {
                        return;
                    }

                    @Override
                    public void onDataReceived(JSONObject object) {
                        Toast.makeText(AddEditActivity.this, "Recording stopped", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onDataReceived(byte[] bytes) {
                        return;
                    }

                    @Override
                    public void onDataReceived(Bitmap bitmap) {
                        return;
                    }

                    @Override
                    public void onFailure(JSONObject object) {
                        failureToast.show();
                    }

                    @Override
                    public void onFailure(byte[] bytes) {
                        return;
                    }
                });
            }
        });
    }

    /**
     * Method that will check that we have the permissions to take a picture
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == CAMERA_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // we can use the camera
                this.takePicture();
            } else {
                mTakePictureButton.setEnabled(false);
            }
        }
    }

    /**
     * Takes a picture for the recipe
     */
    private void takePicture(){
        tempPic = new File(getFilesDir(), mRecipeID + ".jpg");
        try{
            tempPic.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }

        Context context = AddEditActivity.this;
        Uri outputUri = FileProvider.getUriForFile( context, context.getApplicationContext().getPackageName() + ".provider", tempPic);

        Intent takeAPicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takeAPicIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

        startActivityForResult(takeAPicIntent, PHOTO_CODE);
    }

    /**
     * Called when the picture has been taken or cancelled
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PHOTO_CODE && resultCode == RESULT_OK){
            Log.d(TAG, "Picture saved");
        }
    }

    /**
     * Overrides the back pressed to return to the main activity
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddEditActivity.this, MainActivity.class);
        startActivity(intent);
        AddEditActivity.this.finish();
    }

    /**
     * Get the different values from the different fields.
     * @return -> request params based on a JSON object that contains the values in the different text boxes.
     */
    private RequestParams getValues(){
        JSONObject params = new JSONObject();
        try {
            /**
             * Check if title field is empty
             */
            if (!mRecipeTitle.getText().toString().equals("")) {
                params.put("recipeTitle", mRecipeTitle.getText().toString());
            } else {
                params.put("recipeTitle", mEditingRecipe.getRecipeTitle());
            }
            /**
             * Check if ingredients field is empty
             */
            if (!mRecipeIngredients.getText().toString().equals("")) {
                // attributes for building / updating a new recipe
                List<String> mRecipe_Ingredients = StringFormatter.formatStringToList(mRecipeIngredients.getText().toString());
                JSONArray jsonArray = new JSONArray(mRecipe_Ingredients);
                params.put("recipeIngredients", jsonArray);
            } else {
                params.put("recipeIngredients", new JSONArray(mEditingRecipe.getRecipeIngredients()));
            }
            /**
             * Check it method field is empty
             */
            if (!mRecipeMethod.getText().toString().equals("")) {
                List<String> mRecipe_Method = StringFormatter.formatStringToList(mRecipeMethod.getText().toString());
                JSONArray jsonArray = new JSONArray(mRecipe_Method);
                params.put("recipeMethod", jsonArray);
            } else {
                params.put("recipeMethod", new JSONArray(mEditingRecipe.getRecipeMethod()));
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date date = new Date();
            params.put("createdAt", dateFormat.format(date));

        } catch(JSONException e){
            e.printStackTrace();
        }
        return JsonHelper.toRequestParams(params);
    }
}