package be.richeza.maistreq_mobile.controllers;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.models.helpers.ApiClientHandler;

/**
 * This class is the base activity that i will use throughout the application.
 * It handles the theme + the coordinates for the api, etc
 */
public class BaseActivity extends AppCompatActivity {

    public static final String TAG = "BaseActivity";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setting preferences based on the shared preferences
        PreferenceManager.setDefaultValues(this, R.xml.root_preferences, false);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        // Getting API URL and PORT
        ApiClientHandler.apiURL = sharedPref.getString("Api_URL", ApiClientHandler.DefaultApiConfiguration.apiURL);
        ApiClientHandler.apiPORT = sharedPref.getString("Api_PORT", ApiClientHandler.DefaultApiConfiguration.apiPORT);

        // Getting the chosen theme -> this theme handler is not compatible with all the versions.
        // (It depends on the manufacturer, but it can be run if there is a "dark mode" option somewhere)
        boolean switchPref = sharedPref.getBoolean(SettingsActivity.themeSwitcher, false);
        if(switchPref) {
            Log.d(TAG, "applying dark theme");
            this.setTheme(android.R.style.Theme_DeviceDefault_DayNight);
        }
        else {
            Log.d(TAG, "applying light theme");
            this.setTheme(android.R.style.Theme_DeviceDefault_Light_DarkActionBar);
        }
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }
}
