package be.richeza.maistreq_mobile.controllers;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.models.Recipe;
import be.richeza.maistreq_mobile.models.RecipeBook;
import be.richeza.maistreq_mobile.models.helpers.ApiMiddleware;
import be.richeza.maistreq_mobile.models.helpers.StringFormatter;
import be.richeza.maistreq_mobile.models.exceptions.RecipeNotFoundException;
import be.richeza.maistreq_mobile.models.helpers.interfaces.Callback;

/**
 * This controller handles the detailed view
 */
public class DetailedViewActivity extends BaseActivity {

    public static String TAG = "DetailedViewActivity";

    // Members
    private Recipe mChosenRecipe;
    private FileOutputStream mFileOutputStream;
    private ApiMiddleware mApiCaller;
    private Boolean isVideoFound;

    // Widgets
    private TextView mRecipeTitle;
    private TextView mRecipeDate;
    private TextView mRecipeIngredients;
    private TextView mRecipeContent;
    private ImageView mRecipeImage;
    private FloatingActionButton mEditRecipeFAB;
    private TextView mRecipeVideoHeader;
    private Button mRecipeVideoBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_detailed_view);
        this.setUpWidgets();

        mApiCaller = new ApiMiddleware(this);
        isVideoFound = false;

        // Get recipe book instance
        String recipeID = getIntent().getSerializableExtra("recipe_id").toString();
        Log.d(TAG, "Recipe ID: " + recipeID);

        // Retrieve the recipe from our local storage
        try{
            mChosenRecipe = RecipeBook.get(this).getRecipe(recipeID);
            setUpView();
            this.getRecipeImage(mChosenRecipe.getObjectID());
            this.getVideo(mChosenRecipe.getObjectID());
        } catch(RecipeNotFoundException e){
            Log.d(TAG, "Recipe not found for id:" + recipeID);
        }
    }

    /**
     * get the image based on the recipe ID
     * @param recipeID -> ID of the recipe that we want the image from
     */
    private void getRecipeImage(String recipeID){
        mApiCaller.getImage(recipeID, new Callback() {
            @Override
            public void onDataReceived(List<Recipe> recipes) {
                return;
            }

            @Override
            public void onDataReceived(JSONObject object) {
                return;
            }

            @Override
            public void onDataReceived(byte[] bytes) {
                return;
            }

            @Override
            public void onDataReceived(Bitmap bitmap) {
                Log.d(TAG, "image received");
                Matrix matrix = new Matrix();
                matrix.setRotate(90);
                Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                mRecipeImage.setImageBitmap(rotatedBitmap);

            }

            @Override
            public void onFailure(JSONObject object) {
                return;
            }

            @Override
            public void onFailure(byte[] bytes) {
            }
        });
    }

    private void getVideo(String recipeID){
        final File file = new File(getFilesDir(), mChosenRecipe.getObjectID() + ".mp4");
        if(!file.exists()) {
            mApiCaller.getVideo(recipeID, new Callback() {
                @Override
                public void onDataReceived(List<Recipe> recipes) {
                    return;
                }

                @Override
                public void onDataReceived(JSONObject object) {
                    return;
                }

                @Override
                public void onDataReceived(byte[] bytes) {
                    // save it and use it
                    Log.d(TAG, "starting download");
                    Log.d(TAG, "file created");
                    try{
                        file.createNewFile();
                        mFileOutputStream = new FileOutputStream(file);
                        mFileOutputStream.write(bytes);
                        mFileOutputStream.close();
                        Log.d(TAG, "writing done, video.mp4 saved");
                        isVideoFound = true;
                        showVideoView();
                    } catch(IOException e){
                        Log.d(TAG, "Couldn't save video");
                    }

                }

                @Override
                public void onDataReceived(Bitmap bitmap) {
                    return;
                }


                @Override
                public void onFailure(JSONObject object) {
                    return;
                }

                @Override
                public void onFailure(byte[] bytes) {
                    // No mp4 video
                    isVideoFound = false;
                    showVideoView();
                }
            });
        } else {
            // Video does exists
            Log.d(TAG, "video exists");
            isVideoFound = true;
            showVideoView();
        }
    }

    /**
     * Setting up widgets
     */
    private void setUpWidgets(){
        this.mRecipeTitle = findViewById(R.id.recipe_title);
        this.mRecipeDate = findViewById(R.id.recipe_date);
        this.mRecipeIngredients = findViewById(R.id.recipe_ingredients);
        this.mRecipeContent = findViewById(R.id.recipe_content);
        this.mRecipeImage = findViewById(R.id.recipe_image);
        this.mEditRecipeFAB = findViewById(R.id.edit_fab);
        this.mRecipeVideoHeader = findViewById(R.id.recipe_video_label);
        this.mRecipeVideoBtn = findViewById(R.id.recipe_video_btn);
    }

    /**
     * Set up the detailed view
     */
    private void setUpView(){
        this.mRecipeTitle.setText(mChosenRecipe.getRecipeTitle());
        this.mRecipeDate.setText(StringFormatter.concatDate(getString(R.string.recipe_issued_text), mChosenRecipe.getCreatedAt()));
        this.mRecipeIngredients.setText(StringFormatter.formatListToString(mChosenRecipe.getRecipeIngredients()));
        this.mRecipeContent.setText(StringFormatter.formatListToString(mChosenRecipe.getRecipeMethod()));
        this.mRecipeImage.setImageResource(R.drawable.unknownpic);

        this.mEditRecipeFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailedViewActivity.this, AddEditActivity.class);
                intent.putExtra("recipe_id", mChosenRecipe.getObjectID());
                startActivity(intent);
                DetailedViewActivity.this.finish();
            }
        });
        if(!isVideoFound){
            this.mRecipeVideoHeader.setVisibility(View.GONE);
            this.mRecipeVideoBtn.setVisibility(View.GONE);
        }
    }

    /**
     * Sets up and shows up the button that will show the video
     */
    private void showVideoView(){
        if(isVideoFound) {
            this.mRecipeVideoHeader.setVisibility(View.VISIBLE);
            this.mRecipeVideoBtn.setVisibility(View.VISIBLE);
            Log.d(TAG, "buttons supposed to be shown");
            this.mRecipeVideoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToVideoView();
                }
            });
        }
    }

    /**
     * Handles the navigation to the video view
     */
    private void goToVideoView(){
        Intent intent = new Intent(this, RecipeVideoViewActivity.class);
        intent.putExtra("recipe_id", mChosenRecipe.getObjectID());
        startActivity(intent);
    }
}


