package be.richeza.maistreq_mobile.models;


import androidx.core.content.FileProvider;

/**
 * This class is required to permits the application to store images locally
 */
public class GenericFileProvider extends FileProvider {
}
