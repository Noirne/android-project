package be.richeza.maistreq_mobile.ui.configure;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.android.material.snackbar.Snackbar;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.models.helpers.ApiClientHandler;

/**
 * This fragment is used to set the API endpoint
 */
public class ConfigurationFragment extends Fragment {

    // Widgets
    private EditText apiURLText;
    private EditText apiPORTText;
    private Button apiPrefSaverButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_config, container, false);

        this.setUpWidgets(root);

        // Verify if the widgets are empty, if it is, take them from SharedPreferences
        if(apiURLText.getText().length() == 0) apiURLText.setText(ApiClientHandler.apiURL);
        if(apiPORTText.getText().length() == 0) apiPORTText.setText(ApiClientHandler.apiPORT);

        this.setUpSaveButton();

        return root;
    }

    /**
     * Sets up the event handler for the save button
     */
    private void setUpSaveButton() {
        apiPrefSaverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToPreferences();
            }
        });
    }

    /**
     * Sets up the widgets for being able to manipulate them
     * @param root : View
     */
    private void setUpWidgets(View root) {
        apiURLText = root.findViewById(R.id.apiURL);
        apiPORTText = root.findViewById(R.id.apiPORT);
        apiPrefSaverButton = root.findViewById(R.id.save_api_pref_button);
    }

    /**
     * Uses SharedPreferences to set the URL and PORT.
     */
    public void saveToPreferences(){

        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();

        prefEditor.putString("Api_URL", this.getApiURL());
        prefEditor.putString("Api_PORT", this.getApiPort());
        prefEditor.apply();

        this.showAppliedSnackbar();
        getActivity().recreate();
    }

    /**
     * Get the API URL from the widget
     * @return the text contained in the widget
     */
    private String getApiURL(){
        return apiURLText.getText().toString();
    }

    /**
     * Get the API PORT from the widget
     * @return the text contained in the widget
     */
    private String getApiPort(){
        return apiPORTText.getText().toString();
    }

    /**
     * Sets up and show the Snackbar for the save confirmation
     */
    private void showAppliedSnackbar(){
        Snackbar snackbar = Snackbar.make( getView(), "Succesfully saved the changes", Snackbar.LENGTH_LONG).setAction("Action", null);
        View sbView = snackbar.getView();
        // ContextCompat.getColor(getApplicationContext(), R.color.)
        sbView.setBackgroundColor(Color.rgb(0,128,0));
        snackbar.show();
    }


}