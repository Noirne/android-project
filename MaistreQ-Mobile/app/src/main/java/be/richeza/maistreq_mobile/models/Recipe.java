package be.richeza.maistreq_mobile.models;

import android.graphics.Bitmap;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * This model represents a recipe
 */
public class Recipe {

    // Members
    private String objectID;
    private List<String> recipeIngredients;
    private List<String> recipeMethod;
    private Date createdAt;
    private DateFormat dateFormat;
    private String recipeTitle;

    public Recipe(String objectID, List<String> recipeIngredients, List<String> recipeMethod, Date createdAt, String recipeTitle){
        this.objectID = objectID;
        this.recipeIngredients = recipeIngredients;
        this.recipeMethod = recipeMethod;
        this.createdAt = createdAt;
        this.recipeTitle = recipeTitle;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    // GETTERS
    public String getObjectID() {
        return objectID;
    }

    public List<String> getRecipeIngredients() {
        return recipeIngredients;
    }

    public List<String> getRecipeMethod() {
        return recipeMethod;
    }

    public String getCreatedAt() {
        return dateFormat.format(createdAt);
    }

    public String getRecipeTitle() {
        return recipeTitle;
    }
}
