package be.richeza.maistreq_mobile.models.exceptions;

/**
 * This exception is thrown when there is no recipe found
 */
public class RecipeNotFoundException extends RuntimeException {
}
