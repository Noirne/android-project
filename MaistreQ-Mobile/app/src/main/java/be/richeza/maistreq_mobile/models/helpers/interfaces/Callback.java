package be.richeza.maistreq_mobile.models.helpers.interfaces;

import android.graphics.Bitmap;

import org.json.JSONObject;

import java.util.List;

import be.richeza.maistreq_mobile.models.Recipe;

/**
 * This interface is used when we process asynchronous requests to the api
 */
public interface Callback {
    void onDataReceived(List<Recipe> recipes);
    void onDataReceived(JSONObject object);
    void onDataReceived(byte[] bytes);
    void onDataReceived(Bitmap bitmap);
    void onFailure(JSONObject object);
    void onFailure(byte[] bytes);
}
