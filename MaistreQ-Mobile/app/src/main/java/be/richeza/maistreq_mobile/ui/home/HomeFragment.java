package be.richeza.maistreq_mobile.ui.home;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.controllers.DetailedViewActivity;
import be.richeza.maistreq_mobile.models.Recipe;
import be.richeza.maistreq_mobile.models.RecipeBook;
import be.richeza.maistreq_mobile.models.helpers.ApiClientHandler;
import be.richeza.maistreq_mobile.models.helpers.ApiMiddleware;
import be.richeza.maistreq_mobile.models.helpers.interfaces.Callback;
import cz.msebera.android.httpclient.Header;

/**
 * This fragment is containing the list of all our recipes
 */
public class HomeFragment extends Fragment {

    public static String TAG = "HomeFragment";

    // Widgets
    private RecyclerView mRecipesRecyclerView;
    private RecipesAdapter mRecipesAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    // Members
    private List<Recipe> mRecipes;
    private  RecipeBook mRecipeBook;
    private  ApiClientHandler mApiClientHandler;
    private ApiMiddleware mApiMiddleware;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        // Sets up the recycler view
        mRecipesRecyclerView = root.findViewById(R.id.recipes_recycler_view);
        mSwipeRefreshLayout = root.findViewById(R.id.swipe_container);
        mRecipesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Sets up the api callers
        mApiClientHandler = ApiClientHandler.get(this.getContext());
        mApiMiddleware = new ApiMiddleware(this.getContext());

        // Sets up the recipes + the swipe refresh
        mRecipeBook = RecipeBook.get(this.getContext());
        this.mRecipes = new ArrayList<>();
        this.setUpSwipeRefresh();
        return root;
    }

    /**
     * Sets up the swipe refresh to permit us to refresh the recipe list
     */
    private void setUpSwipeRefresh(){
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    /**
     * Updates the UI
     */
    private void updateUI(){
        mApiClientHandler.getServerWorks(new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if(getView() != null){
                    Snackbar snackbar = Snackbar.make( getView(), "Connected to server", Snackbar.LENGTH_LONG).setAction("Action", null);
                    View sbView = snackbar.getView();
                    // ContextCompat.getColor(getApplicationContext(), R.color.)
                    sbView.setBackgroundColor(Color.rgb(0,128,0));
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if(getView() != null){
                    Snackbar snackbar = Snackbar.make( getView(), "Cannot contact the server, try again later", Snackbar.LENGTH_LONG).setAction("Action", null);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(Color.rgb(128, 0,0));
                    snackbar.show();
                }
            }
        });
        mRecipeBook.getmRecipes(new Callback() {
            @Override
            public void onDataReceived(List<Recipe> recipes) {
                mRecipes = recipes;
                if(mRecipesAdapter == null){
                    mRecipesAdapter = new RecipesAdapter(getContext(), mRecipes);
                    mRecipesRecyclerView.setAdapter(mRecipesAdapter);
                } else {
                    mRecipesAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onDataReceived(JSONObject object) {
                return;
            }

            @Override
            public void onDataReceived(byte[] bytes) {
                return;
            }

            @Override
            public void onDataReceived(Bitmap bitmap) {
                return;
            }

            @Override
            public void onFailure(JSONObject object) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Cannot contact the database");
                builder.setMessage("Recipes could not be found");
                builder.show();
            }

            @Override
            public void onFailure(byte[] bytes) {
                return;
            }
        });
    }

    /**
     * Method that is called to make apparent the little circle when we refresh the view
     */
    private void refresh(){
        mSwipeRefreshLayout.setRefreshing(true);
        this.updateUI();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Implicitly refreshes the view when resuming the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "RESUMING");
        this.refresh();
    }

    /**
     * This class handles a unit of the recyclerView + its related widgets
     */
    private class RecipesHolder extends RecyclerView.ViewHolder{
        private Recipe mRecipe;
        private TextView mTitleTextView;
        private TextView mDateTextView;

        public RecipesHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.recipe_list_layout, parent, false));
            mTitleTextView = itemView.findViewById(R.id.recipe_title);
            mDateTextView = itemView.findViewById(R.id.recipe_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: Switch to detailed sheet
                    Intent intent = new Intent(getActivity(), DetailedViewActivity.class);
                    intent.putExtra("recipe_id", mRecipe.getObjectID());
                    startActivity(intent);

                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = setUpAlertDialog();
                    builder.show();
                    return false;
                }
            });
        }

        private AlertDialog.Builder setUpAlertDialog(){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getString(R.string.delete_recipe_title) + " " + mRecipe.getRecipeTitle());
            builder.setIcon(R.drawable.ic_delete_sweep_red_24dp);
            builder.setMessage(R.string.ask_to_delete_recipe);

            this.setButtons(builder);


            return builder;
        }

        /**
         * Handler that asks the user if he wants to delete a recipe or not
         * @param builder
         */
        private void setButtons(AlertDialog.Builder builder){
            builder.setPositiveButton(getString(R.string.string_OK), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // send api request to delete the recipe
                    mApiMiddleware.deleteRecipe(mRecipe.getObjectID(), new Callback() {
                        @Override
                        public void onDataReceived(List<Recipe> recipes) {
                            return;
                        }

                        @Override
                        public void onDataReceived(JSONObject object) {
                            // Log response from api
                            Log.d(TAG, object.toString() + " deleted");
                        }

                        @Override
                        public void onDataReceived(byte[] bytes) {
                            return;
                        }

                        @Override
                        public void onDataReceived(Bitmap bitmap) {
                            return;
                        }

                        @Override
                        public void onFailure(JSONObject object) {

                        }

                        @Override
                        public void onFailure(byte[] bytes) {
                            return;
                        }
                    });
                    HomeFragment.this.getActivity().recreate();
                }
            }).setNegativeButton(getString(R.string.string_CANCEL), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });

        }

        /**
         * Binds a recipe to its widgets
         * @param recipe
         */
        public void bind(Recipe recipe){
            this.mRecipe = recipe;
            mTitleTextView.setText(mRecipe.getRecipeTitle());
            mDateTextView.setText(mRecipe.getCreatedAt());
        }
    }

    /**
     * This class is used to bind all of the little units (Holders) to its recipes
     */
    private class RecipesAdapter extends RecyclerView.Adapter<RecipesHolder>{

        // Members
        private List<Recipe> mRecipes;
        private LayoutInflater mInflater;

        public RecipesAdapter(Context ctxt, List<Recipe> recipes){
            this.mInflater = LayoutInflater.from(ctxt);
            this.mRecipes = recipes;
        }

        @NonNull
        @Override
        public RecipesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RecipesHolder(mInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull RecipesHolder holder, int position) {
            Recipe recipe = mRecipes.get(position);
            holder.bind(recipe);
        }

        @Override
        public int getItemCount() {
            return this.mRecipes.size();
        }
    }
}