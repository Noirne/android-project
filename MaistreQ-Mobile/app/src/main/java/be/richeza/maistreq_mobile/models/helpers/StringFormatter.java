package be.richeza.maistreq_mobile.models.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This helper is used to format Strings in a certain format
 */
public class StringFormatter {

    public static final String TAG = "StringFormatter";

    /**
     * Takes a list of string and returns it with dashes in front of each element
     * @param list : List -> the list of string we want
     * @return A multiline string
     */
    public static String formatListToString(List<String> list){
        StringBuilder tempStringToReturn = new StringBuilder();

        for(String s: list){
            tempStringToReturn.append("- " + s + System.lineSeparator());
        }

        return tempStringToReturn.toString();
    }

    /**
     * Takes a formatted string and returns a list of string
     * @param stringToBuild : String -> A multiline String with a certain format
     * @return a list of string
     */
    public static ArrayList<String> formatStringToList(String stringToBuild){
        ArrayList<String> multiStringToReturn;
        stringToBuild  = stringToBuild.replaceAll(System.lineSeparator(), "");
        multiStringToReturn = new ArrayList(Arrays.asList(stringToBuild.split("-")));
        if(multiStringToReturn.get(0).equals("")){
            multiStringToReturn.remove(0);
        }
        return multiStringToReturn;
    }

    /**
     * Concat a date
     * @param issuedText : String -> the first part of the formulation
     * @param createdAt : String -> the date
     * @return the concatenation of both parameters
     */
    public static String concatDate(String issuedText,String createdAt){
        return issuedText + " " + createdAt;
    }

}
