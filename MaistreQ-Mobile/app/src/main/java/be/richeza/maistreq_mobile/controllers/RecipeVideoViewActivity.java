package be.richeza.maistreq_mobile.controllers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.models.RecipeBook;

public class RecipeVideoViewActivity extends AppCompatActivity {

    public static final String TAG = "RecipeVideoViewActivity";

    // Widgets
    private VideoView mRecipeVideoView;
    private MediaController controller;

    // Video position
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_video_view);

        position = 0;

        if(savedInstanceState != null){
            this.recoverBundleState(savedInstanceState);
        }

        setUpWidgets();
        setUpAttributes();

        String recipeID = getIntent().getStringExtra("recipe_id");
        setTitle("Video: " + RecipeBook.get(this).getRecipe(recipeID).getRecipeTitle());

        // The videos are stored locally at this time, we, then, can access them in the app folder
        if(recipeID != null){
          this.setUpVideo(recipeID);
        }

    }

    /**
     * Sets up the video view
     * @param recipeID  : String (Mongoose UID) -> Used to find the associated video in the app folder
     */
    private void setUpVideo(String recipeID){
        mRecipeVideoView.setVideoURI(Uri.parse(new File(getFilesDir(), recipeID + ".mp4").getAbsolutePath()));
        mRecipeVideoView.setMediaController(controller);
        controller.setAnchorView(mRecipeVideoView);
        Log.d(TAG, "position: " + position);
        mRecipeVideoView.seekTo(position);
        mRecipeVideoView.setSaveEnabled(true);
        mRecipeVideoView.start();

        // This thread keeps track of the video position each 10 seconds
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    do{
                        Thread.sleep(10000);
                        position = mRecipeVideoView.getCurrentPosition();
                    }while(mRecipeVideoView.getCurrentPosition() != 0);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }

            }
        }).start();
    }

    /**
     * Recovers the bundle state if we modify the orientation of the video
     * @param savedInstanceBundle : Bundle -> bundle that is recovered when the activity is recreated
     */
    private void recoverBundleState(Bundle savedInstanceBundle){
        position = savedInstanceBundle.getInt("current_position");
        Log.d(TAG, "position after saving: " + position);
    }

    /**
     * Called when we change the orientation of the activity
     * @param outState : Bundle -> The bundle that will be recovered
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("current_position", position);
        controller.hide();
    }

    /**
     * Sets up the widget
     */
    private void setUpWidgets(){
        mRecipeVideoView = findViewById(R.id.recipe_video_view);
    }

    /**
     * Sets up the attribute
     */
    private void setUpAttributes(){
        controller = new MediaController(this);
    }
}
