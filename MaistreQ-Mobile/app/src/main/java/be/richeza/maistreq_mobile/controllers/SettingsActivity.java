package be.richeza.maistreq_mobile.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.preference.PreferenceFragmentCompat;

import be.richeza.maistreq_mobile.R;

/**
 * This is the controller of the settings activity in charge of the dark theme
 */
public class SettingsActivity extends BaseActivity {

    // Members
    public static final String themeSwitcher = "switch_theme";
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        // Initialize the fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        // Permits us to return to the previous activity
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        intent = new Intent(this, MainActivity.class);
    }

    /**
     * This is the settings fragment that we are using to initialize the preferences
     */
    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }

    }

    /**
     * Overrides a certain item selected because i had to go back to the main activity
     * @param item : MenuItem -> the item we clicked on
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Goes back to the main
     */
    @Override
    public void onBackPressed() {
        startActivity(intent);
        //this.finish();
    }
}