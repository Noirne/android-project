package be.richeza.maistreq_mobile.models;

import android.content.Context;
import android.graphics.Bitmap;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import be.richeza.maistreq_mobile.models.exceptions.RecipeNotFoundException;
import be.richeza.maistreq_mobile.models.helpers.interfaces.Callback;
import be.richeza.maistreq_mobile.models.helpers.ApiMiddleware;

/**
 * This model represents the local storage of our recipes
 */
public class RecipeBook {

    public static final String TAG = "RecipeBook";

    private static RecipeBook sRecipeBook;

    // Members
    private ArrayList<Recipe> mRecipes;
    private Context mContext;
    private ApiMiddleware mApiMiddleware;

    /**
     * Singleton
     * @param ctxt
     * @return a new RecipeBook or the recipe book if it already exists
     */
    public static RecipeBook get(Context ctxt){
        if(sRecipeBook == null) sRecipeBook = new RecipeBook(ctxt);
        return sRecipeBook;
    }

    private RecipeBook(Context ctxt){
        this.mContext = ctxt;
        this.mApiMiddleware = new ApiMiddleware(mContext);
        this.mRecipes = new ArrayList<>();
    }

    /**
     * Get the recipes from the database
     * @return a list of recipes
     */
    public void getmRecipes(final Callback callback){
        mApiMiddleware.getRecipes(new Callback() {
            @Override
            public void onDataReceived(List<Recipe> recipes) {
                mRecipes = (ArrayList<Recipe>) recipes;
                callback.onDataReceived(mRecipes);
            }

            @Override
            public void onDataReceived(JSONObject object) {
                return;
            }

            @Override
            public void onDataReceived(byte[] bytes) {
                return;
            }

            @Override
            public void onDataReceived(Bitmap bitmap) {
                return;
            }

            @Override
            public void onFailure(JSONObject object) {
                callback.onFailure(object);
            }

            @Override
            public void onFailure(byte[] bytes) {
                return;
            }
        });
    }

    /**
     * get a certain recipe from local storage
     * @param recipeID : String (Mongoose UID) -> the recipe's ID that we want
     * @return the Recipe linked to the ID
     */
    public Recipe getRecipe(String recipeID){
        for (Recipe recipe: this.mRecipes) {
            if(recipe.getObjectID().equals(recipeID) ) {
                return recipe;
            }
        }
        throw new RecipeNotFoundException();
    }

}
