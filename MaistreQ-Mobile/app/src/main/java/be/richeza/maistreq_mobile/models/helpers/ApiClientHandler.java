package be.richeza.maistreq_mobile.models.helpers;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.OutputStream;

/**
 * This class is handling the requests to the api
 */
public class ApiClientHandler {

    // Api endpoint -> set by Base Activity
    public static String apiURL = "";
    public static String apiPORT = "";

    // Members
    private Context mContext;
    public static String TAG = "ApiClientHandler";
    private AsyncHttpClient httpClient;
    private static ApiClientHandler handler;

    // Singleton
    public static ApiClientHandler get(Context context){
        if(handler == null) handler = new ApiClientHandler(context);
        return handler;
    }

    private ApiClientHandler(Context context){
        this.mContext = context;
        this.httpClient = new AsyncHttpClient();
    }

    /**
     * Get a check to see if the api is running
     * @param responseHandler : AsyncHttpResponseHandler -> Handles the response
     */
    public void getServerWorks(AsyncHttpResponseHandler responseHandler){
        httpClient.get(getAbsoluteUrl("/"), new RequestParams(), responseHandler);
    }

    /**
     * Get all the recipes found in the database
     * @param responseHandler : AsyncHttpResponseHandler -> Set the handling of the response received
     */
    public void getAllRecipes(AsyncHttpResponseHandler responseHandler){
        httpClient.get(getAbsoluteUrl("recipes/"), new RequestParams(), responseHandler);
    }

    /**
     * Get the image corresponding to the recipe from the database
     * @param recipeID : String (Mongoose UID) -> the ID of a recipe
     * @param responseHandler : AsyncHttpResponseHandler -> Set the handling of the response received
     */
    public void getImage(String recipeID, AsyncHttpResponseHandler responseHandler){
        httpClient.get(getAbsoluteUrl("recipes/getImage/" + recipeID), new RequestParams(), responseHandler);
    }

    /**
     * Starts a record linked to the recipe we're actually modifying
     * @param recipeID : String (Mongoose UID) -> the ID of the recipe that we want to record the video.mp4 for
     * @param responseHandler : AsyncHttpResponseHandler -> Handles the response received
     */
    public void startVideo(String recipeID, AsyncHttpResponseHandler responseHandler){
        httpClient.get(getAbsoluteUrl("records/startRecord/" + recipeID), new RequestParams(), responseHandler);
    }

    /**
     * Stops a record linked to a recipe
     * @param recipeID : String (Mongoose UID) -> the ID of a recipe
     * @param responseHandler : AsyncHttpResponseHandler -> Handles the response received
     */
    public void stopVideo(String recipeID, AsyncHttpResponseHandler responseHandler){
        httpClient.get(getAbsoluteUrl("records/stopRecord/" + recipeID), new RequestParams(), responseHandler);
    }

    /**
     * Get the video.mp4 corresponding to the recipe
     * @param recipeID : String (Mongoose UID) -> the ID of the recipe that we want the video.mp4 from
     * @param responseHandler : AsyncHttpResponseHandler -> Set the handling of the response received
     */
    public void getVideo(String recipeID, AsyncHttpResponseHandler responseHandler){
        httpClient.get(getAbsoluteUrl("records/getVideo/" + recipeID), new RequestParams(), responseHandler);
    }

    /**
     * Add a new recipe to the database
     * @param params : RequestParams -> Send the data about the recipe in request params
     * @param responseHandler : AsyncHttpResponseHandler -> Set the handling of the response received
     */
    public void postRecipe(RequestParams params, AsyncHttpResponseHandler responseHandler){
        httpClient.post(mContext, getAbsoluteUrl("recipes/addRecipe"), params, responseHandler);
    }


    /**
     * Update a recipe in the database
     * @param params : RequestParams ->  Send the data about the recipe.
     * @param recipeID : String (Mongoose UID) -> To retrieve which object to update
     * @param responseHandler : AsyncHttpResponseHandler  -> Set the handling of the response received
     */
    public void updateRecipe(RequestParams params, String recipeID, AsyncHttpResponseHandler responseHandler){
        httpClient.post(mContext, getAbsoluteUrl("recipes/updateRecipe/" + recipeID), params, responseHandler);
    }

    /**
     * Add an image corresponding to a recipe in the database
     * @param params : RequestParams -> the picture to send
     * @param responseHandler : AsyncHttpResponseHandler  -> Set the handling of the response received
     */
    public void postImage(String recipeID, RequestParams params, AsyncHttpResponseHandler responseHandler){
        httpClient.post(getAbsoluteUrl("recipes/addImage/" + recipeID), params, responseHandler);
    }

    /**
     * Deletes a recipe
     * @param recipeID : String (Mongoose UID) -> the recipeID of the recipeID that we want to delete
     * @param responseHandler
     */
    public void deleteRecipe(String recipeID, AsyncHttpResponseHandler responseHandler){
        httpClient.delete(getAbsoluteUrl("recipes/" + recipeID), new RequestParams(), responseHandler);
    }

    /**
     * This method is used to get the absoluteURL by concatenating the URL and port the section wanted
     * @param relativeUrl
     * @return
     */
    private static String getAbsoluteUrl(String relativeUrl){
        return apiURL + ":" + apiPORT + "/" + relativeUrl;
    }

    /**
     * This class is used to give default values if there is no preferences found
     */
    public static class DefaultApiConfiguration {
        public static String apiURL = "http://192.168.178.252";
        public static String apiPORT = "3000";
    }
}
