package be.richeza.maistreq_mobile.models.helpers;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import be.richeza.maistreq_mobile.R;
import be.richeza.maistreq_mobile.models.Recipe;
import be.richeza.maistreq_mobile.models.helpers.interfaces.Callback;
import cz.msebera.android.httpclient.Header;

/**
 * This is an helper that will call the API, format the data and send them to the different models
 */
public class ApiMiddleware {
    public static final String TAG = "ApiMiddleware";

    private ApiClientHandler handler;
    private Context mContext;

    public ApiMiddleware(Context ctxt){
        this.mContext = ctxt;
        this.handler = ApiClientHandler.get(this.mContext);
    }

    /**
     * Get the recipes.
     * @return a list containing all the recipes in the database.
     */
    public void getRecipes(final Callback callback) {
        final List<Recipe> recipesToReturn = new ArrayList<>();

        handler.getAllRecipes(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    JSONObject responseObject;
                    for (int i = 0; i < response.length(); i++) {
                        responseObject = response.getJSONObject(i);

                        JSONArray ingredientsArray = responseObject.getJSONArray("recipeIngredients");
                        JSONArray methodArray = responseObject.getJSONArray("recipeMethod");

                        String responseDate = responseObject.getString("createdAt");
                        String createdAtDateString = responseDate.split("T")[0];
                        Date createdAtDate = new SimpleDateFormat(mContext.getString(R.string.dateFormatTemplate)).parse(createdAtDateString);

                        List<String> recipeIngredients = ApiMiddleware.this.getJsonArray(ingredientsArray);
                        List<String> recipeMethod = ApiMiddleware.this.getJsonArray(methodArray);

                        recipesToReturn.add(new Recipe(responseObject.getString("_id"), recipeIngredients, recipeMethod, createdAtDate, responseObject.getString("recipeTitle")));
                    }
                    callback.onDataReceived(recipesToReturn);
                } catch (JSONException e) {
                    Log.d(TAG, "Error while parsing JSON:\n" + e);
                } catch (ParseException es) {
                    Log.d(TAG, "Object received was not expected");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(errorResponse);
            }
        });
    }

    /**
     * Takes a json array and returns a list of string
     * @param jsonArray : The json array that we want to take strings out of it
     * @return a list of string
     */
    public List<String> getJsonArray(JSONArray jsonArray){
        List<String> tempListToReturn = new ArrayList<>();

        try{
            for(int i = 0; i < jsonArray.length(); i++){
                tempListToReturn.add(jsonArray.get(i).toString());
            }
            return tempListToReturn;
        } catch(JSONException e){
            Log.e(TAG, "Exception while parsing the json", e);
        }
        return null;
    }

    /**
     * Takes params to update a recipe online
     * @param params
     * @param recipeID
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void updateRecipe(RequestParams params, String recipeID, final Callback callback){
        handler.updateRecipe(params, recipeID, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onDataReceived(response);
            }
        });
    }

    /**
     * Takes params to add a new recipe
     * @param params
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void addRecipe(RequestParams params, final Callback callback){
        handler.postRecipe(params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onDataReceived(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(errorResponse);
            }
        });
    }

    /**
     * Starts the record for a certain recipe
     * @param recipeID : String (Mongoose UID) -> The id of the recipe that we want to shoot the video for
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void startRecord(String recipeID, final Callback callback){
        Log.d(TAG, "Start record function");
        handler.startVideo(recipeID, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, "Start record success");
                callback.onDataReceived(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.e(TAG, "start record fail");
                callback.onFailure(errorResponse);
            }
        });
    }

    /**
     * Stops the record for a certain recipe
     * @param recipeID : String (Mongoose UID) -> The id of the recipe that we want to shoot the video for
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void stopRecord(String recipeID, final Callback callback){
        handler.stopVideo(recipeID, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onDataReceived(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(errorResponse);
            }
        });
    }

    /**
     * Gets the video associated to the recipe ID
     * @param recipeID : String (Mongoose UID) -> The recipe's ID that we want the video from
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void getVideo(String recipeID, final Callback callback){
        handler.getVideo(recipeID, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                callback.onDataReceived(responseBody);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onFailure(responseBody);
            }
        });
    }

    /**
     * Gets the image associated to the recipe ID
     * @param recipeID : String (Mongoose UID) -> The ID of the recipe we want the picture from
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void getImage(String recipeID, final Callback callback){
        handler.getImage(recipeID, new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d(TAG, "success image");
                callback.onDataReceived(BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(TAG, "fail image");
                callback.onFailure(responseBody);
            }
        });
    }

    /**
     * Sends a picture that will be associated to a recipe
     * @param recipeID : String (Mongoose UID) -> The recipe's ID that we want to associate with the picture
     * @param params : Byte stream -> The picture that we will send
     * @param callback Interface that will be called when we got a response from an asynchronous request
     */
    public void addImage(String recipeID, RequestParams params, final Callback callback){
        handler.postImage(recipeID , params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onDataReceived(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(errorResponse);
            }
        });
    }

    /**
     * Deletes a recipe
     * @param recipeID : String (Mongoose UID) -> The recipe that we want to delete
     * @param callback : Interface that will be called when we got a response from an asynchronous request
     */
    public void deleteRecipe(String recipeID, final Callback callback){
        handler.deleteRecipe(recipeID, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onDataReceived(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onFailure(errorResponse);
            }
        });
    }
}
