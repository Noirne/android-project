package be.richeza.maistreq_mobile.models.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * This class is used to format a file (image) to byte64 format ready to send online
 */
public class PictureFormatter {

    public static String ConvertToBase64(File file){
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
        byte [] imgBytes = outputStream.toByteArray();

        return Base64.encodeToString(imgBytes, Base64.NO_WRAP);
    }
}
